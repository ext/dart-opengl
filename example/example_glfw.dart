import 'dart:ffi';

import 'package:glfw/glfw.dart';
import 'package:opengl/opengl.dart';
import 'package:ffi/ffi.dart';

void main() {
  glfwInit();
  print('GLFW: ${glfwGetVersionString().cast<Utf8>().toDartString()}');

  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  final window = glfwCreateWindow(640, 480, 'Dart FFI + GLFW + OpenGL'.toNativeUtf8(),
      nullptr, nullptr);
  glfwMakeContextCurrent(window);

  print('GL_VENDOR: ${glGetString(GL_VENDOR).cast<Utf8>().toDartString()}');
  print('GL_RENDERER: ${glGetString(GL_RENDERER).cast<Utf8>().toDartString()}');
  print('GL_VERSION: ${glGetString(GL_VERSION).cast<Utf8>().toDartString()}');

  glClearColor(0.0, 0.7, 0.99, 0.0);
  glViewport(0, 0, 600, 400);

  while (glfwWindowShouldClose(window) != GLFW_TRUE) {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glfwSwapBuffers(window);
    glfwPollEvents();
  }
}
